package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Country;

public interface CountryService {
    public Country findCountryByShortName(String shortName);
}
