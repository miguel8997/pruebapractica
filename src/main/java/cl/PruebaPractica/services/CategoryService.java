package cl.PruebaPractica.services;


import cl.PruebaPractica.models.Category;

public interface CategoryService {
    public Category findCategoryById(Integer id);
}
