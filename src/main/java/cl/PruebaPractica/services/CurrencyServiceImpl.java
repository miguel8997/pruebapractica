package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Currency;
import cl.PruebaPractica.repositorys.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CurrencyServiceImpl implements CurrencyService{

    @Autowired
    private CurrencyRepository currencyRepository;

    @Override
    public Currency findCurrencyByShortName(String shortName) {
        return currencyRepository.findByShortName(shortName).orElse(null);
    }
}
