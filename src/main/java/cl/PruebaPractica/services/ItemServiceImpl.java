package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Category;
import cl.PruebaPractica.models.Country;
import cl.PruebaPractica.models.Currency;
import cl.PruebaPractica.models.Item;
import cl.PruebaPractica.payload.ItemRequest;
import cl.PruebaPractica.payload.ItemRequest2;
import cl.PruebaPractica.repositorys.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private CurrencyService currencyService;

    @Autowired
    private CategoryService categoryService;

    @Override
    public Item findItemById(Integer id) {
        return itemRepository.findById(id).orElse(null);
    }

    @Override
    public Item updateItem(Item itemDB, ItemRequest itemRequest) {
        Country country=countryService.findCountryByShortName(itemRequest.getCountry().toUpperCase());
        Currency currency=currencyService.findCurrencyByShortName(itemRequest.getCurrency().toUpperCase());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        itemDB.setModified_at(timestamp);
        itemDB.setTitle(itemRequest.getTitle());
        itemDB.setPrice(itemRequest.getPrice());
        itemDB.setCountry_id(country);
        itemDB.setCurrency_id(currency);
        itemDB.setSymbol(itemDB.getTitle().charAt(0));

        return itemRepository.save(itemDB);
    }

    @Override
    public Item saveItem(ItemRequest2 item) {
        Country country=countryService.findCountryByShortName(item.getCountry().toUpperCase());
        Currency currency=currencyService.findCurrencyByShortName(item.getCurrency().toUpperCase());
        Category category=categoryService.findCategoryById(item.getCategory_id());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Item itemNew=new Item();
        itemNew.setTitle(item.getTitle());
        itemNew.setSymbol(item.getTitle().charAt(0));
        itemNew.setPrice(item.getPrice());
        itemNew.setCountry_id(country);
        itemNew.setCurrency_id(currency);
        itemNew.setCategory_id(category);
        itemNew.setCreated_at(timestamp);

        return itemRepository.save(itemNew);
    }

    @Override
    public String deleteItemById(Integer id) {
        String outPut="Item eliminado correctamente";
        itemRepository.deleteById(id);
        return outPut;
    }
}
