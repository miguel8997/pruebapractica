package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Item;
import cl.PruebaPractica.payload.ItemRequest;
import cl.PruebaPractica.payload.ItemRequest2;

public interface ItemService {
    public Item findItemById(Integer id);
    public Item updateItem(Item itemDB, ItemRequest itemRequest);
    public Item saveItem(ItemRequest2 item);
    public String deleteItemById(Integer id);
}
