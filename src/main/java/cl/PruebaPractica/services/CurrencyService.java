package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Currency;

public interface CurrencyService {
    public Currency findCurrencyByShortName(String shortName);
}
