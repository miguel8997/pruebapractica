package cl.PruebaPractica.services;

import cl.PruebaPractica.models.Country;
import cl.PruebaPractica.repositorys.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryServiceImpl implements CountryService{

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public Country findCountryByShortName(String shortName) {
        return countryRepository.findByShortName(shortName).orElse(null);
    }
}
