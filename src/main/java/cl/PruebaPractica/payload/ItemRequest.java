package cl.PruebaPractica.payload;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ItemRequest {

    @NotBlank(message = "Title cannot be empty")
    @Size(max = 100)
    private String title;

    @Positive
    private double price;

    @NotNull(message = "Country cannot be empty")
    @Size(max = 3)
    private String country;

    @NotNull(message = "Currency cannot be empty")
    @Size(max = 3)
    private String currency;

    public ItemRequest(@NotBlank(message = "Title cannot be empty") @Size(max = 100) String title,
                       @Positive double price,
                       @NotNull(message = "Country cannot be empty") @Size(max = 3) String country,
                       @NotNull(message = "Currency cannot be empty")
                       @Size(max = 3) String currency) {
        this.title = title;
        this.price = price;
        this.country = country;
        this.currency = currency;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
