package cl.PruebaPractica.payload;

import javax.validation.constraints.*;

public class ItemRequest2 {

    @NotBlank(message = "Title cannot be empty")
    @Size(max = 100)
    private String title;

    @Positive
    private double price;

    @NotBlank(message = "Country cannot be empty")
    @Size(max = 3)
    private String country;

    @NotBlank(message = "Currency cannot be empty")
    @Size(max = 3)
    private String currency;

    @NotNull(message = "Category_id may not be null")
    private int category_id;


    public ItemRequest2(@NotBlank(message = "Title cannot be empty") @Size(max = 100) String title,
                        @Positive double price,
                        @NotBlank(message = "Country cannot be empty") @Size(max = 3) String country,
                        @NotBlank(message = "Currency cannot be empty") @Size(max = 3) String currency,
                        @NotNull(message = "Category_id may not be null") int category_id) {
        this.title = title;
        this.price = price;
        this.country = country;
        this.currency = currency;
        this.category_id = category_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

}
