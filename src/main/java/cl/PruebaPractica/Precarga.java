package cl.PruebaPractica;

import cl.PruebaPractica.models.Category;
import cl.PruebaPractica.models.Country;
import cl.PruebaPractica.models.Currency;
import cl.PruebaPractica.models.Item;
import cl.PruebaPractica.repositorys.CategoryRepository;
import cl.PruebaPractica.repositorys.CountryRepository;
import cl.PruebaPractica.repositorys.CurrencyRepository;
import cl.PruebaPractica.repositorys.ItemRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.sql.Timestamp;

@Configuration
public class Precarga {

    @Bean(name = "currency")
    CommandLineRunner cargaCurrency(CurrencyRepository currencyRepository){
        return(args->{
            currencyRepository.save(new Currency("CLP","Moneda Chilena"));
            currencyRepository.save(new Currency("ARS","Peso Argentino"));
        });
    }

    @Bean(name = "country")
    CommandLineRunner cargaCountry(CountryRepository countryRepository){
        return(args->{
            countryRepository.save(new Country("CL","Chile"));
            countryRepository.save(new Country("ARG","Argentina"));
        });
    }

    @Bean(name = "category")
    CommandLineRunner cargaCategory(CategoryRepository categoryRepository){
        return(args->{
            categoryRepository.save(new Category("alimentos"));
        });
    }

    @Bean(name = "item")
    CommandLineRunner cargaItem(ItemRepository itemRepository,
                                CategoryRepository categoryRepository,
                                CountryRepository countryRepository,
                                CurrencyRepository currencyRepository){

        return(args->{
            Timestamp t=new Timestamp(System.currentTimeMillis());
            itemRepository.save(new Item("Platano",
                    categoryRepository.findById(1).orElse(null),
                    200,
                    'P',
                    currencyRepository.findById(1).orElse(null),
                    countryRepository.findById(1).orElse(null),
                    t));
        });
    }
}
