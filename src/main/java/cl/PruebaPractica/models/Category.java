package cl.PruebaPractica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(	name = "category")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Name may not be null")
    @Size(max = 100)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "category_id")
    @JsonIgnoreProperties("category")
    private Set<Item> items;

    public Category() {
    }

    public Category(@NotNull(message = "Name may not be null") @Size(max = 100) String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
