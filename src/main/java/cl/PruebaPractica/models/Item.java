package cl.PruebaPractica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.sql.Timestamp;

@Entity
@Table(	name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Title cannot be empty")
    @Size(max = 100)
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    @JsonIgnoreProperties("items")
    private Category category_id;   

    @Positive
    private double price;

    @NotNull(message = "Symbol cannot be empty")
    private Character symbol;

    @ManyToOne
    @NotNull(message = "Currency cannot be empty")
    @JoinColumn(name = "currency_id")
    @JsonIgnoreProperties("items")
    private Currency currency_id;

    @ManyToOne
    @NotNull(message = "Country cannot be empty")
    @JoinColumn(name = "country_id")
    @JsonIgnoreProperties("items")
    private Country country_id;

    @NotNull(message = "Country cannot be empty")
    private Timestamp created_at;

    private Timestamp modified_at;

    public Item() {
    }

    public Item(@NotBlank(message = "Title cannot be empty") @Size(max = 100) String title,
                Category category_id,
                @Positive double price,
                @NotNull(message = "Symbol cannot be empty") Character symbol,
                @NotNull(message = "Currency cannot be empty") Currency currency_id,
                @NotNull(message = "Country cannot be empty") Country country_id,
                @NotNull(message = "Country cannot be empty") Timestamp created_at) {
        this.title = title;
        this.category_id = category_id;
        this.price = price;
        this.symbol = symbol;
        this.currency_id = currency_id;
        this.country_id = country_id;
        this.created_at = created_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory_id() {
        return category_id;
    }

    public void setCategory_id(Category category_id) {
        this.category_id = category_id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Character getSymbol() {
        return symbol;
    }

    public void setSymbol(Character symbol) {
        this.symbol = symbol;
    }

    public Currency getCurrency_id() {
        return currency_id;
    }

    public void setCurrency_id(Currency currency_id) {
        this.currency_id = currency_id;
    }

    public Country getCountry_id() {
        return country_id;
    }

    public void setCountry_id(Country country_id) {
        this.country_id = country_id;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getModified_at() {
        return modified_at;
    }

    public void setModified_at(Timestamp modified_at) {
        this.modified_at = modified_at;
    }
}
