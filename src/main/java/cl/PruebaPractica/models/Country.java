package cl.PruebaPractica.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Entity
@Table(	name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Short name may not be null")
    @Size(max = 3)
    private String shortName;

    @NotNull(message = "Long name may not be null")
    @Size(max = 100)
    private String longName;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "country_id")
    @JsonIgnoreProperties("country")
    private Set<Item> items;

    public Country() {
    }

    public Country(@NotNull(message = "Short name may not be null") @Size(max = 3) String shortName,
                   @NotNull(message = "Long name may not be null") @Size(max = 100) String longName) {
        this.shortName = shortName;
        this.longName = longName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public Set<Item> getItems() {
        return items;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }
}
