package cl.PruebaPractica.controllers;

import cl.PruebaPractica.models.Item;
import cl.PruebaPractica.payload.ItemRequest;
import cl.PruebaPractica.payload.ItemRequest2;
import cl.PruebaPractica.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/api/item")
public class ItemController {

    @Autowired
    private ItemService itemService;

    @GetMapping("/{id}")
    public ResponseEntity<Item> findItemById(@PathVariable Integer id){
        Item item=itemService.findItemById(id);
        if (item==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(item);
    }

    @PatchMapping("/updateItem/{id}")        //<----PatchMapping
    public ResponseEntity<Item> updateItem(@PathVariable Integer id, @Valid @RequestBody ItemRequest itemRequest){
        Item itemDB=itemService.findItemById(id);
        if (itemDB==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(itemService.updateItem(itemDB,itemRequest));
    }

    @DeleteMapping("/{id}")
    public Map deleteItem(@PathVariable Integer id){
        Item itemDB=itemService.findItemById(id);
        if (itemDB==null){
            return Collections.singletonMap("response", "Item no encontrado");
        }
        return Collections.singletonMap("response", itemService.deleteItemById(id));
    }

    @PostMapping("/addItem")
    public Map addItem(@Valid @RequestBody ItemRequest2 item){
        return Collections.singletonMap("id", itemService.saveItem(item).getId());
    }
}
