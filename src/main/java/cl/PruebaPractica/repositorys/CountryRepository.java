package cl.PruebaPractica.repositorys;

import cl.PruebaPractica.models.Category;
import cl.PruebaPractica.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CountryRepository extends JpaRepository<Country,Integer> {
    Optional<Country> findByShortName(String shortName);
}
